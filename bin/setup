#!/usr/bin/env bash

# Colorize
C=$(printf '\033')
W="${C}[1;37m"
R="${C}[1;31m"
B="${C}[1;34m"
G="${C}[1;32m"
T="${C}[1;36m"
Y="${C}[1;33m"
RST="${C}[1;0m"

echo "Welcome to tokenizr!"
echo "Setting up config file now"

# Setup will obtain information from the tokenizr root directory
cd $(dirname "$0"); cd ..

CONFIG_FILE=$(pwd)/tokenizr.cfg

write_config() {
	echo "##############################################################" > "${CONFIG_FILE}"
	echo "#" >> "${CONFIG_FILE}"
	echo "# TOKENIZR - An art generation tool" >> "${CONFIG_FILE}"
	echo "#" >> "${CONFIG_FILE}"
	echo "##############################################################" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# GLOBAL VARIABLES - PLEASE CHANGE AS NECESSARY" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# NAME OF COLLECTION" >> "${CONFIG_FILE}"
	echo "COLLECTION_NAME=My_Project" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# TOKENIZR HOME DIRECTORY" >> "${CONFIG_FILE}"
	echo "PROJECT_PATH=$(pwd)" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# NUMBER OF TOKENS TO GENERATE" >> "${CONFIG_FILE}"
	echo "TOKEN_NUM=10" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# RARITY OF MULTIMEDIA TOKENS (AS A PERCENT OF TOTAL GENERATED TOKENS)" >> "${CONFIG_FILE}"
	echo "MULTIMEDIA_RARITY=0" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# BOOLEAN - INCLUDE RARE FOLDER OF 1-OF-1 ART WHEN GENERATING ART ('0' is FALSE, '1' IS TRUE)" >> "${CONFIG_FILE}"
	echo "INCLUDE_RARES=0" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF GENERATED TOKENS" >> "${CONFIG_FILE}"
	echo "TOKEN_PATH=$(pwd)/build/images" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# USE WEIGHTED RANDOM SELECTION FOR IMAGE CHOICES (WARNING: changing this value in config prevents toggling it via art_engine)" >> "${CONFIG_FILE}"
	echo "WEIGHTED_SELECTION=0" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# TEMP DIRECTORY (used for temporary file generation and removal)" >> "${CONFIG_FILE}"
	echo "TMP_DIR=$(pwd)/tmp" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOG FILE (mainly for ffmpeg output)" >> "${CONFIG_FILE}"
	echo "LOG_FILE=$(pwd)/log/output.log" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "# VISUAL ART SECTION" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF VISUAL TRAITS LAYERS" >> "${CONFIG_FILE}"
	echo "IMAGE_LAYERS_PATH=$(pwd)/traits/images" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "LOCATION OF RARE ART TO BE GENERATED" >> "${CONFIG_FILE}"
	echo "RARE_ART_PATH=$(pwd)/rare" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "# ANIMATED ART SECTION" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF ANIMATED (mp4) TRAITS LAYERS" >> "${CONFIG_FILE}"
	echo "ANIMATED_LAYERS_PATH=$(pwd)/traits/video" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "# AUDIO ART SECTION" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF AUDIO TRAITS LAYERS" >> "${CONFIG_FILE}"
	echo "AUDIO_LAYERS_PATH=$(pwd)/traits/audio" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF GENERATED AUDIO FILES" >> "${CONFIG_FILE}"
	echo "AUDIO_FILE_PATH=$(pwd)/build/audio" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "# METADATA SECTION" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# LOCATION OF GENERATED JSON FILES" >> "${CONFIG_FILE}"
	echo "JSON_PATH=$(pwd)/build/json" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# JSON DESCRIPTION" >> "${CONFIG_FILE}"
	echo "JSON_DESCRIPTION=My_Project_Collection" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# EDITION NUMBER" >> "${CONFIG_FILE}"
	echo "EDITION_NUM=1" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# CID HASH (HASH OF GENERATED IMAGES FOLDER)" >> "${CONFIG_FILE}"
	echo "CID_VALUE=CHANGE_THIS_VALUE" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "# HIDDEN CID HASH (HASH OF HIDDEN METADATA FILE)" >> "${CONFIG_FILE}"
	echo "HIDDEN_CID_VALUE=CHANGE_THIS_VALUE" >> "${CONFIG_FILE}"
	echo "" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "# IMAGE TRAITS ORDER" >> "${CONFIG_FILE}"
	echo "#############################################################################" >> "${CONFIG_FILE}"
	echo "" >> ${CONFIG_FILE}
	echo "# CURRENT ORDER:" >> "${CONFIG_FILE}"
	echo -n "TRAITS_ORDER=" >> "${CONFIG_FILE}"
	if [ $traits_num ]; then
		echo -n "(" >> "${CONFIG_FILE}"
		for num in $(seq 0 $((  ${traits_num} - 1 ))); do
			[ $num -eq $(( $traits_num - 1 )) ] && { echo -n "$num" >> "${CONFIG_FILE}"; break; }	# If last element, remove final ' '
			echo -n "$num " >> "${CONFIG_FILE}"
		done
	fi
	echo ")" >> "${CONFIG_FILE}"
}



main() {
	ffmpeg -h >&/dev/null; [ $? -eq 0 ] || { echo -e "Missing dependency: ffmpeg\nExiting."; exit 1; }
	convert -version >&/dev/null; [ $? -eq 0 ] || { echo -e "Missing dependency: converf (image magick)\nExiting."; exit 1; }

	# Perform overwrite check
	if [[ -f "${CONFIG_FILE}" ]]; then
		answer='N'
		echo "File: "${CONFIG_FILE}" already exists!"
		read -p "Overwrite with default config file? (y/N): " answer
		if [[ "$answer" == 'n' ]] || [[ "$answer" == 'N' ]]; then
			echo "Exiting"
			exit 1
		fi
	fi
	
	# Get number of traits
	traits_num=$(find ./traits/images/* -type d | wc -w)

	write_config
}

main
