#!/usr/bin/env bash



# Analyze metadata files and print percentage of each image asset used in a collection

# Move to script directory to find config files, etc.
cd "$(dirname "$0")" || exit 1
source ../lib/tokenizr.shlib

# Global vars
IMAGE_LAYERS_PATH=$(config_get IMAGE_LAYERS_PATH)
JSON_PATH=$(config_get JSON_PATH)
WEIGHTED_SELECTION=$(config_get WEIGHTED_SELECTION)

# Colorize
C=$(printf '\033')
W="${C}[1;37m"
R="${C}[1;31m"
B="${C}[1;34m"
G="${C}[1;32m"
T="${C}[1;36m"
Y="${C}[1;33m"
RST="${C}[1;0m"

LOG_FILE=../log/percentages.log

# Build array of asset directories
build_category_list() {
	for directory in "${IMAGE_LAYERS_PATH}"/*; do
		category+="$(basename ${directory}) "
	done
}

# Build array of every asset (including duplicates) from every JSON file in a given asset category
build_assets_list() {
	for file in "${JSON_PATH}"/*; do
		for line in $(grep "${keyword}" -A1 $file | tail -1 | cut -d: -f2 | tr -d "" | sed 's/"//g'); do
			container+="$line "
		done
		(( total_assets+=1 ))
	done
}

# Used in printing all assets from a given category (used with sort and uniq to generate a list of uniq assets)
print_container() {
	for cell in ${container[@]}; do
		echo $cell
	done
}

# Generate a list of every asset category list and the percent usage of each asset in said category (with non-color output also being sent to ../log/percentage.log)
main() {
	declare -a category			# Array of image asset categories that will be used as the 'keywords' for build_asset_list()

	build_category_list

	echo "" && echo -e "$(date)\n" > ${LOG_FILE}
	echo "${T}##############################################" && echo "##############################################" >> ${LOG_FILE}
	echo -e "${W}ASSETS\t\t\tTOTAL\t(%)\tWEIGHT" && echo -e "ASSETS\t\t\tTOTAL\t(%)\tWEIGHT" >> ${LOG_FILE}
	echo "${T}##############################################" &&echo "##############################################" >> ${LOG_FILE}
	echo ""

	# For each image asset category
	for keyword in ${category[@]}; do
		declare -a container 		# Array of every generated asset (including all repeats)
		declare -A asset_percents	# Associative array of each unique asset (per asset category) and the corresponding number of times it was generated in the collection
		total_assets=0				# Total number of assets (i.e. # of total tokens generated in the collection)
		build_assets_list

		# Initialize our unique assets totals
		for asset in $(print_container | sort | uniq); do
			asset_percents[$asset]=0
		done

		# Iterate through full asset list
		for asset in ${container[@]}; do

			# For each asset in full list, iterate through our unique list and increment the matching asset by 1
			for key in ${!asset_percents[@]}; do
				if [ ${asset} = $key ]; then
					(( asset_percents[$key]+=1 ))
				fi
			done
		done

		echo -e "${T}- ${W}${keyword} ${T}-\t\t---\t---\t---" && echo "- ${keyword} -\t\t---\t---\t---" >> ${LOG_FILE}
		echo "" | tee -a ${LOG_FILE}

		# Iterate through our associative assets array, convert each value to a percent, and print
		for asset in ${!asset_percents[@]}; do

			# Use awk to perform floating-point division and convert to percent
			percent=$(awk -v pct=${asset_percents[$asset]} "BEGIN{print(pct/${total_assets}*100)}")

			# Use weight from asset filenames for easy side-by-side comparisons
			weight=$(ls ../traits/images/${keyword} | grep -iE "^${asset}-[0-9]{1,3}.png$" | sed -E 's/.*-([0-9]{1,3}).png$/\1/')

			# Format string for clean output
			printf '%-24s\t%s\t%s\t%s\n' "${Y}${asset}" "${T}${asset_percents[$asset]}" "${T}${percent}%" "${T}${weight}" && printf '%-24s\t%s\t%s\t%s\n' "${asset}" "${asset_percents[$asset]}" "${percent}%" "${weight}" >> ${LOG_FILE}
		done
		echo "" | tee -a ${LOG_FILE}

		# Unset arrays to allow for re-initializing
		unset container
		unset asset_percents
	done
}

main





