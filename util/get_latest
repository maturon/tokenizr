#!/usr/bin/env bash

# Colorize
C=$(printf '\033')
W="${C}[1;37m"
R="${C}[1;31m"
B="${C}[1;34m"
G="${C}[1;32m"
T="${C}[1;36m"
Y="${C}[1;33m"
RST="${C}[1;0m"

# A quick and dirty rename and re-clone tool utilizing git

# Move to script's directory to find config files, etc.
cd $(dirname "$0")
cd ../..

# If tokenizr doesn't exist (if running this script without the rest of the software) then clone the latest tokenizr
current_tokenizr=$(ls | grep -E '^tokenizr$')
[ -d ${current_tokenizr} ] || { echo "${R}[-] ${W}- No previous tokenizr directory found. Obtaining latest tokenizr."; git clone https://gitlab.com/maturon/tokenizr.git; exit 0; }

# If tokenizr.old exists, remove it
old_tokenizr="${current_tokenizr}.old"
[ -d ${old_tokenizr} ] && rm -rf ${old_tokenizr}

# Move current version ot tokenizr to tokenizr.old,clone the latest version, and remove the empty directories to make copying image/audio assets possible
mv ${current_tokenizr} ${old_tokenizr}
git clone https://gitlab.com/maturon/tokenizr && echo "${G}[+] ${W}- Successfully obtained latest version of tokenizr!"
rm -rf tokenizr/{rare,traits,build} && echo "${G}[+] ${W}- Removing empty folders and preparing to migrate assets."

# Copy config settings
cp ${old_tokenizr}/tokenizr.cfg ${current_tokenizr}/tokenizr.cfg && echo "${G}[+] ${W}- Copying config settings"

# Move image/audio assets to our new tokenizr
mv ${old_tokenizr}/build/ ${current_tokenizr}/ && echo "${G}[+] ${W}- Migrating build folder"
mv ${old_tokenizr}/traits/ ${current_tokenizr}/ && echo "${G}[+] ${W}- Migrating traits folder"
mv ${old_tokenizr}/rare/ ${current_tokenizr}/ && echo "${G}[+] ${W}- Migrating rare folder"

# Exit script outside of the tokenizr directories
echo "${W}Don't forget to switch to your new downloaded tokenizr directory before proceeding ${T}(cd ../tokenizr)${RST}"