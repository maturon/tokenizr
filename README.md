# Tokenizr - An Art Generation Engine

## About The Project

[Tokenizr][8]

Tokenizr is a command-line utility written in bash that pieces together static as well as animated images along with audio to generate one-of-a-kind (non-fungible) art and corresponding metadata. This project has been designed specifically for the Wabbeats NFT project but may later be adapted for not-so-specifc generative art uses.

<p align="right">(<a href="#top">back to top</a>)</p>

## Prerequisites

Tokenizr is a native linux utility so any dependency installation will be distribution specific. Aside from bash, Tokenizr utilizes ImageMagick and ffmpeg applications to piece together audio and visual components. To install these utilities, please see different package manager installation methods below:

For Debian/GNU Linux Distros (such as Debian, Ubuntu, etc.)
  ```sh
  sudo apt update
  sudo apt install ffmpeg imagemagick
  ```

For Redhat Linux Distros (such as Fedora, CentOS, etc.):
  ```sh
  sudo yum install ffmpeg ImageMagick
  ```

For Arch Linux Distros (such as Arch, Manjaro, etc.):
  ```sh
  sudo pacman -Sy ffmpeg imagemagick
  ```

For Mac, Homebrew is the recommended method. To install Homebrew, open a terminal and run the following commands:
  ```sh
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  ```

Then run:
  ```sh
  brew update
  brew install ffmpeg imagemagick
  ```

## Installation and Setup

1. Open a terminal and use git to download the latest version:
```sh
git clone https://gitlab.com/maturon/tokenizr
```
2. Move all art assets into the tokenizr/traits/images folder (make sure each asset class has its own folder, ex. Backgrounds, Hair, Eyes, etc.).
3. Move any audio assets into the tokenizr/traits/audio folder (make sure each asset class has its own folder, like the images above).
4. Move any rare 1-of-1 assets into the tokenizr/rare folder (these images don't require separate folders since they are unique and non-generated).

<p align="right">(<a href="#top">back to top</a>)</p>

## Conventions

There are a couple areas where conventions are required in order for the script to execute without issues. These conventions are namely:

Asset names (images) - Please make sure all image names are weighted (generally between 1 and 100) using the naming convention '-num' after the name but before the extention, for example: bad_azzzz_eyeball-75.png or 'GiAnT hAir-20.png'. There is a temporary script to [correct filenames][7] until the code is modified to take this into account.

Currently, audio assets are not weighted so feel free to name them whatever you want (please be sure they are in .m4a or .mp4 format, though).


## Usage

1. Simply run the interactive menu script in the root directory!
```sh
cd tokenizr
./tokenizr
```
2. If the setup script has not been run, tokenizr will run setup and auto-gen a default tokenizr.cfg file. The menu options will provide you with a front-end to utilize the tools in the bin directory.
3. The bin tools can be used independently of the tokenizr interactive menu. If you are more comfortable (or it is more convenient) running the tools separately, please feel free.
4. Main menu options:
  - Generate art tokens (via [art_engine][1])
  - Generate audio files (via [audio_engine][2])
  - Update metadata files (via [metadata_engine][3])
  - Delete all generated art (via [reset_build][4])
  - Create a .tar.gz file of the current tokenizr application without art assets (via [make_distro][5])
  - Update itself via git (via [get_latest][6])

<!-- LICENSE -->
## License

Distributed under the GPLv3 License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

Maturon Miner - maturon@gmail.com

<p align="right">(<a href="#top">back to top</a>)</p>

[1]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/art_engine
[2]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/audio_engine
[3]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/metadata_engine
[4]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/reset_build
[5]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/make_distro
[6]: https://gitlab.com/maturon/tokenizr/-/tree/main/bin/get_latest
[7]: https://gitlab.com/maturon/tokenizr/-/tree/main/correct_filenames
[8]: https://gitlab.com/maturon/tokenizr