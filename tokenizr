#!/bin/bash

version=0.3.1

OS_NAME=$(uname -s)

# Move to tokenizr directory
cd $(dirname "$0")

# Check for config file and run setup if non-existant
[ -f "./tokenizr.cfg" ] || { echo "[+] No config file detected. Running setup."; bin/setup; }

source ./lib/tokenizr.shlib

# Obtain all of our config file variables
include_rare=$(config_get INCLUDE_RARES)
audio_dir=$(config_get AUDIO_FILE_PATH)
audio_traits_dir=$(config_get AUDIO_LAYERS_PATH)
token_dir=$(config_get TOKEN_PATH)
images_dir=$(config_get IMAGE_LAYERS_PATH)
json_dir=$(config_get JSON_PATH)
token_num=$(config_get TOKEN_NUM)
use_weight=$(config_get WEIGHTED_SELECTION)
description=$(config_get JSON_DESCRIPTION)
edition=$(config_get EDITION_NUM)
cid_value=$(config_get CID_VALUE)
hidden_cid_value=$(config_get HIDDEN_CID_VALUE)
config_file=$(pwd)/tokenizr.cfg



# Colorize
C=$(printf '\033')
W="${C}[1;37m"
R="${C}[1;31m"
B="${C}[1;34m"
G="${C}[1;32m"
T="${C}[1;36m"
Y="${C}[1;33m"
RST="${C}[1;0m"

print_banner() {
	echo ""
	echo "${R}WELCOME TO THE"
	echo "                 ___           ___           ___           ___                       ___           ___     "
	echo "     ___        /  /\         /__/|         /  /\         /__/\        ___          /  /\         /  /\    "
	echo "    /  /\      /  /::\       |  |:|        /  /:/_        \  \:\      /  /\        /  /::|       /  /::\   "
	echo "   /  /:/     /  /:/\:\      |  |:|       /  /:/ /\        \  \:\    /  /:/       /  /:/:|      /  /:/\:\  "
	echo "  /  /:/     /  /:/  \:\   __|  |:|      /  /:/ /:/_   _____\__\:\  /__/::\      /  /:/|:|__   /  /:/~/:/  "
	echo " /  /::\    /__/:/ \__\:\ /__/\_|:|____ /__/:/ /:/ /\ /__/::::::::\ \__\/\:\__  /__/:/ |:| /\ /__/:/ /:/___"
	echo "/__/:/\:\   \  \:\ /  /:/ \  \:\/:::::/ \  \:\/:/ /:/ \  \:\~~\~~\/    \  \:\/\ \__\/  |:|/:/ \  \:\/:::::/"
	echo "\__\/  \:\   \  \:\  /:/   \  \::/~~~~   \  \::/ /:/   \  \:\  ~~~      \__\::/     |  |:/:/   \  \::/~~~~ "
	echo "     \  \:\   \  \:\/:/     \  \:\        \  \:\/:/     \  \:\          /__/:/      |  |::/     \  \:\     "
	echo "      \__\/    \  \::/       \  \:\        \  \::/       \  \:\         \__\/       |  |:/       \  \:\    "
	echo "                \__\/         \__\/         \__\/         \__\/                     |__|/         \__\/    "
	echo ""
	echo "                                                                         INTERACTIVE MENU - Version: ${version}"
	echo ""
}

# Clear current line (stdout)
clear_line() {
	printf '\r'
	cols="$(tput cols)"
	for i in $(seq "$cols"); do
		printf ' '
	done
	printf '\r'
}

# Clear menu from stdout, arg is number of lines to clear
clear_menu() {
	test -z "$1" && lines="1" || lines="$1"

	up='\033[1A'

	[ "$lines" = 0 ] && return

	if [ "$lines" = 1 ]; then
		clear_line
	else
		(( lines-=1 ))
		clear_line
		for i in $(seq "$lines"); do
			printf "$up"
			clear_line
		done
	fi
}

main() {

	# Regexs for testing input validity
	is_num='^[0-9]+$'
	is_letter='^[a-zA-Z]{1}$'
	has_letters='[a-zA-Z]+'

	print_banner

	# Main menu loop
	while :; do
		echo "${T}MAIN MENU"
		echo "-----------"
		echo "${T} s${Y}) - Static art token generation"
		echo "${T} a${Y}) - Audio token generation"
		echo "${T} m${Y}) - Multimedia token generation"
		echo "${T} M${Y}) - Mixed media (static and multimedia) token generation"
		echo "${T} u${Y}) - Update existing metadata files in bulk"
		echo "${T} d${Y}) - Delete all previously generated art in build folder"
		echo "${T} z${Y}) - Zip tokenizr for quick file distribution"
		echo "${T} g${Y}) - Get latest version of tokenizr via git"
		echo "${T} q${Y}) - Quit"
		echo ""
		read -p "${T}Option: ${W}" selection
		clear_menu 14

		case "${selection}" in
			s)
				generate_static_art
				;;
			a)
				generate_audio
				;;
			m)
				generate_multimedia_art
				;;
			M)
				generate_mixed_media_art
				;;
			u)
				update_CIDs
				;;
			d)
				clear_generated_art
				;;
			z)
				quick_package
				;;
			g)
				get_latest
				;;
			q)
				clear_menu 17
				exit 0
				;;
			?)
				continue
				;;
		esac
	done
}

# Function that collects a string of args for art_engine and runs art_engine
generate_static_art() {

	# Build this string through user input to be passed to various bin scripts
	options_string=

	while :; do
		echo -e "${T}STATIC ART ENGINE - MENU\t\t\t\tVALUE"
		echo -e "----------------------------------\t\t\t-----"
		echo -e "${T} t${Y}) - Set number of tokens to generate\t\t\t${W}${token_num}"
		echo -e "${T} w${Y}) - Weighted selection toggle\t\t\t\t${W}${use_weight}"
		echo -e "${T} r${Y}) - Rare folder toggle ('0' = off | '1' = on)\t\t${W}${include_rare}"
		echo -e "${T} i${Y}) - image traits sub-directories root\t\t\t${W}${images_dir}"
		echo -e "${T} o${Y}) - Set new token output directory\t\t\t${W}${token_folder}"
		echo -e "${T} c${Y}) - Change/Update order of traits sub-directories\t\t\t${W}${token_folder}"
		echo -e "${T} g${Y}) - Generate collection"
		echo "${T} b${Y}) - Back"
		echo ""
		read -p "${T}Option: ${W}" selection

		# Make sure menu selection is single letter
		[[ ${selection} =~ $is_letter ]] || { clear_menu 13; continue; }

		case "${selection}" in
			t)
				while :; do
					read -p "${T}Number of tokens to generate: ${W}" token_num

					# Error check: input is a number >= zero
					if [[ ! ${token_num} =~ $is_num ]] || [ ${token_num} -lt 0 ]; then
						clear_menu 3
						echo "${R}Invalid input."
						continue
					fi
					clear_menu 14
					break
				done
				;;
			w)
				# Toggle option on or off depending on previous state
				[ ${use_weight} = 0 ] && use_weight=1 || use_weight=0
				clear_menu 13
				;;
			r)
				# Toggle option on or off depending on previous state
				[ ${include_rare} = 0 ] && include_rare=1 || include_rare=0
				clear_menu 13
				;;
			i)
				while :; do
					read -p "${T}Change images root directory: ${W}" images_dir

					# Error check: output directory exists
					if [ ! -d "${images_dir}" ]; then
						clear_menu 3
						echo "${R}Unable to locate images directory."
						continue
					fi
					clear_menu 14
					break
				done
				;;
			o)
				while :; do
					read -p "${T}Change token output directory: ${W}" token_dir

					# Error check: output directory exists
					if [ ! -d "${token_dir}" ]; then
						clear_menu 3
						echo "${R}Unable to locate output directory."
						continue
					fi
					clear_menu 14
					break
				done
				;;
			c)
				clear_menu 13
				setup_traits
				;;
			g)
				# Build our CLI args for art_engine
				options_string="-t ${token_num} -i ${images_dir} -o ${token_dir}"
				[ ${include_rare} = 1 ] && options_string="${options_string} -r"
				[ ${use_weight} = 1 ] && options_string="${options_string} -w"

				# Use token_num as an indicator for how many lines to clear with $line_count
				echo "${G}[+] ${W}Generating static art..."
				bin/static_art_engine ${options_string}
				clear_menu $(( 16 + ${token_num} ))
				return
				;;
			b)
				clear_menu 13
				return
				;;
			?)
				clear_menu 13
				continue
				;;
		esac
	done
	clear_menu 11
}

generate_audio() {

	# Build this string through user input to be passed to various bin scripts
	options_string=

	while :; do
		echo -e "${T}AUDIO ENGINE - MENU\t\t\t\t\tVALUE"
		echo -e "----------------------------\t\t\t\t-----"
		echo -e "${T} n${Y}) - Set number of tracks to generate\t\t\t${W}${track_num}"
		echo -e "${T} i${Y}) - set new audio traits directory root\t\t${W}${audio_traits_dir}"
		echo -e "${T} o${Y}) - Set new audio output directory\t\t\t${W}${audio_dir}"
		echo -e "${T} g${Y}) - Generate audio"
		echo "${T} b${Y}) - Back"
		echo ""
		read -p "${T}Option: ${W}" selection

		# Make sure menu selection is single letter
		[[ ${selection} =~ $is_letter ]] || { clear_menu 11; continue; }
		
		case "${selection}" in
			n)
				# Modify number of audio files to generate
				line_count=0
				while :; do
					read -p "${T}Number of audio files to generate: ${W}" track_num

					# Error check: input is a number >= zero
					if [[ ${track_num} =~ $is_num ]] && [ ${track_num} -ge 0 ]; then
						clear_menu $(( 11 + $line_count ))
						break
					fi
					echo "${R} Invalid input."
					(( line_count+=2 ))	
				done
				;;
			i)
				# Modify where audio files are generated
				line_count=0
				while :; do
					read -p "${T}Change audio traits input directory (current dir: ${B}./$(basename $(pwd))${T}): ${W}" audio_traits_dir
					if [ -d "${audio_dir}" ]; then
						clear_menu $(( 11 + $line_count ))
						break
					fi
					echo "${R}Unable to locate output directory."
					(( line_count+=2 ))
				done
				;;
			o)
				# Modify where audio files are generated
				line_count=0
				while :; do
					read -p "${T}Change audio file output directory (current dir: ${B}./$(basename $(pwd))${T}): ${W}" audio_dir
					if [ -d "${audio_dir}" ]; then
						clear_menu $(( 11 + $line_count ))
						break
					fi
					echo "${R}Unable to locate output directory."
					(( line_count+=2 ))
				done
				;;
			g)
				clear_menu 9
				echo "${G}[+] ${W}Generating audio file..."
				bin/audio_engine -n ${track_num} -i ${audio_traits_dir} -o ${audio_dir} >&/dev/null
				clear_menu 3
				return
				;;
			b)
				clear_menu 10
				return
				;;
			?)
				clear_menu 10
				continue
				;;
		esac
	done
}

generate_multimedia_art() {
	
	# Build this string through user input to be passed to various bin scripts
	options_string=

	while :; do
		echo -e "${T}MULTIMEDIA ART ENGINE - MENU\t\t\t\tVALUE"
		echo -e "----------------------------------\t\t\t-----"
		echo -e "${T} t${Y}) - Set number of tokens to generate\t\t\t${W}${token_num}"
		echo -e "${T} o${Y}) - Set new token output directory\t\t\t${W}${token_folder}"
		echo -e "${T} c${Y}) - Change/Update order of traits sub-directories\t\t\t${W}${token_folder}"
		echo -e "${T} g${Y}) - Generate collection"
		echo "${T} b${Y}) - Back"
		echo ""
		read -p "${T}Option: ${W}" selection

		# Make sure menu selection is single letter
		[[ ${selection} =~ $is_letter ]] || { clear_menu 10; continue; }

		case "${selection}" in
			t)
				while :; do
					read -p "${T}Number of tokens to generate: ${W}" token_num

					# Error check: input is a number >= zero
					if [[ ! ${token_num} =~ $is_num ]] || [ ${token_num} -lt 0 ]; then
						clear_menu 3
						echo "${R}Invalid input."
						continue
					fi
					clear_menu 11
					break
				done
				;;
			o)
				while :; do
					read -p "${T}Change token output directory: ${W}" token_dir

					# Error check: output directory exists
					if [ ! -d "${token_dir}" ]; then
						clear_menu 3
						echo "${R}Unable to locate output directory."
						continue
					fi
					clear_menu 11
					break
				done
				;;
			c)
				clear_menu 10
				setup_traits
				;;
			g)
				# Build our CLI args for art_engine
				options_string="-t ${token_num} -o ${token_dir}"

				# Use token_num as an indicator for how many lines to clear with $line_count
				echo "${G}[+] ${W}Generating multimedia art..."
				bin/multimedia_art_engine ${options_string}
				clear_menu 13
				return
				;;
			b)
				clear_menu 10
				return
				;;
			?)
				clear_menu 10
				continue
				;;
		esac
	done
	clear_menu 9
}

generate_mixed_media_art() {

	#Generate random seed entropy (remove preceding zeros with `sed` to prevent base errors) or use jot for Mac OSX
	[ $OS_NAME = "Linux" ] && RANDOM=$(date +%s%N | cut -b10-19 | sed 's/^0*//')
	[ $OS_NAME = "Darwin" ] && RANDOM=$(jot -r 1 1000000000 9999999999)

	# Build this string through user input to be passed to various bin scripts
	options_string=
	multimedia_percent=0

	while :; do
		echo -e "${T}MIXED MEDIA ART ENGINE - MENU\t\t\t\tVALUE"
		echo -e "----------------------------------\t\t\t-----"
		echo -e "${T} t${Y}) - Set number of tokens to generate\t\t\t${W}${token_num}"
		echo -e "${T} w${Y}) - Weighted selection toggle\t\t\t\t${W}${use_weight}"
		echo -e "${T} m${Y}) - Set multimedia token percentage\t\t\t${W}${multimedia_percent}"
		echo -e "${T} o${Y}) - Set new token output directory\t\t\t${W}${token_folder}"
		echo -e "${T} g${Y}) - Generate collection"
		echo "${T} b${Y}) - Back"
		echo ""
		read -p "${T}Option: ${W}" selection

		# Make sure menu selection is single letter
		[[ ${selection} =~ $is_letter ]] || { clear_menu 11; continue; }

		case "${selection}" in
			t)
				while :; do
					read -p "${T}Number of tokens to generate: ${W}" token_num

					# Error check: input is a number >= zero
					if [[ ! ${token_num} =~ $is_num ]] || [ ${token_num} -lt 0 ]; then
						clear_menu 3
						echo "${R}Invalid input."
						continue
					fi
					clear_menu 12
					break
				done
				;;
			w)
				# Toggle option on or off depending on previous state
				[ ${use_weight} = 0 ] && use_weight=1 || use_weight=0
				clear_menu 11
				;;
			m)
				while :; do
					read -p "${T}Multimedia token percent: ${W}" multimedia_percent

					# Error check: input is a number >= zero
					if [[ ! ${multimedia_percent} =~ $is_num ]] || [ ${multimedia_percent} -lt 0 ] || [ ${multimedia_percent} -gt 100 ]; then
						clear_menu 3
						echo "${R}Invalid input."
						continue
					fi
					clear_menu 12
					break
				done	
				;;
			o)
				while :; do
					read -p "${T}Change token output directory: ${W}" token_dir

					# Error check: output directory exists
					if [ ! -d "${token_dir}" ]; then
						clear_menu 3
						echo "${R}Unable to locate output directory."
						continue
					fi
					clear_menu 12
					break
				done
				;;
			g)
				# Build our CLI args for art_engine
				options_string="-o ${token_dir}"
				weight_string=""
				[ ${use_weight} = 1 ] && weight_string="-w"

				for ((token=1; token<=token_num; token++)); do

					# If our random 1-100 roll is greater than 100 - multimedia_percent then generate a multimedia token
					rand_num=$(echo $(( $RANDOM % 101 )))
					if [ ${rand_num} -gt $(( 100 - ${multimedia_percent} )) ]; then
						./bin/multimedia_art_engine -g ${token} ${options_string}
						continue
					fi

					# Default: generate a static art token
					./bin/static_art_engine -g ${token} ${options_string} ${weight_string}
				done

				clear_menu $(( 11 + ${token_num} ))
				return
				;;
			b)
				clear_menu 11
				return
				;;
			?)
				clear_menu 12
				continue
				;;
		esac
	done
}

setup_traits() {
	declare -a traits_dirs
	declare -a traits_order

	line_count=0

	# Save names of traits subdirectories in array
	for directory in $(find ./traits/images/* -type d); do
		traits_dirs[${#traits_dirs[@]}]=$(basename ${directory})
	done

	# Loop through and get the preferred order to combine image traits
	counter=0
	choice=0

	echo "${T}ORDER / RE-ORDER TRAITS DIRECTORIES - MENU"

	for increment in ${!traits_dirs[@]}; do
		echo ""
		# Show subdirs still available to be selected and 'red out' the directories that have already been selected
		for dir_num in ${!traits_dirs[@]}; do
			used=0
			for index in ${traits_order[@]}; do
				if [[ $dir_num == $index ]]; then
					echo "${R}$(( ${dir_num} + 1 ))) ${R}${traits_dirs[$dir_num]} "
					used=1
					break
				fi
			done
			[ $used = 0 ] && echo "${T}$(( ${dir_num} + 1 ))) ${B}${traits_dirs[$dir_num]} "
		done
		echo ""	
		echo ""

		# ERROR CHECKING
		valid=0
		while [ $valid = 0 ]; do
			repeat=0

			# Get input and store in traits_order array
			read -p "${RST}Choose directory #$(( ${counter} + 1 )): " choice
			if [[ ! "${choice}" =~ ${is_num} ]] || [ "${choice}" -lt 1 ] || [ "${choice}" -gt ${#traits_dirs[@]} ]; then
				clear_menu 3
				echo "${R}Invalid choice.${RST}"
				continue
			fi

			for ineligible in ${traits_order[@]}; do
				if [[ $(( $choice - 1 )) == $ineligible ]]; then
					clear_menu 3
					echo "${R}Ineligible choice.${RST}"
					repeat=1
					break
				fi
			done
			[ $repeat -eq 1 ] && continue
			valid=1
		done

		# VALID CHOICE workflow
		clear_menu $(( ${#traits_dirs[@]} + $(line_count) + 7 ))
		traits_order[$counter]=$(( ${choice} - 1 ))
		(( counter+=1 ))

		# Display current order of traits subdirs
		echo -n "Current order: "
		for dir_num in ${traits_order[@]}; do
			echo -n "${B}${traits_dirs[${dir_num}]} ${W}|${B} "
		done
		echo ""

		[ ${counter} -ge ${#traits_dirs[@]} ] && { echo ""; break; }
	done

	clear_menu 3

	# Build string of traits sub-directories in selected order for comment display
	dir_string="# CURRENT ORDER: | "
	for dir_num in ${!traits_order[@]}; do
		dir_string+="${traits_dirs[${traits_order[$dir_num]}]} | "
	done

	# Replace any pre-existing TRAITS_ORDER info and variable
	[ "$OS_NAME" = "Linux" ] && {
		sed -i "s/# CURRENT ORDER:.*$/${dir_string}/" "${config_file}"
		sed -i "s/TRAITS_ORDER=.*$/TRAITS_ORDER=($(echo ${traits_order[@]}))/" "${config_file}"
	}
	[ "$OS_NAME" = "Darwin" ] && {
		sed -i '' "s/# CURRENT ORDER:.*$/${dir_string}/" "${config_file}"
		sed -i '' "s/TRAITS_ORDER=.*$/TRAITS_ORDER=($(echo ${traits_order[@]}))/" "${config_file}"
	}
}

# Delete all all previoulsy generated using tokenizr (generally helpful for testing purposes or starting new projects)
clear_generated_art() {
	wipe_clean='n'
	echo "${R}THIS OPTION WILL DELETE ALL PREVIOUSLY GENERATED ART (INCLUDING METADATA)!"
	echo ""
	line_count=0
	while :; do
		read -p "${W}Continue? (y/N): " wipe_clean
		[ ${wipe_clean} = 'n' ] || [ ${wipe_clean} = 'N' ] && { clear_menu $(( 4 + ${line_count} )); break;  }
		[ ${wipe_clean} = 'y' ] || [ ${wipe_clean} = 'Y' ] && { clear_menu $(( 2 + ${line_count} )); yes | ./util/reset_build >&/dev/null; echo "${G}[+] ${W}- Deleting all recently generated art and metadata"; sleep 2; clear_menu 4; break; }
		echo "${R}${wipe_clean}: option invalid."
		wipe_clean='n'
		(( line_count+=2 ))
	done
}

update_CIDs() {
	while :; do
		echo -e "${T}METADATA MENU\t\t\t\t\t\tVALUE"
		echo -e "-------------\t\t\t\t\t\t-----"
		echo -e "${T} d${Y}) - Description update in metadata\t\t\t${W}${description}"
		echo -e "${T} e${Y}) - Edition number update in metadata\t\t\t${W}${edition}"
		echo -e "${T} u${Y}) - Update CID in regular JSON files\t\t\t${W}${cid_value}"
		echo -e "${T} j${Y}) - JSON hidden file CID update\t\t\t${W}${hidden_cid_value}"
		echo -e "${T} g${Y}) - Generate new metadata from values"
		echo "${T} b${Y}) - Back"
		echo ""
		read -p "${T}Option: ${W}" option

		# Make sure menu selection is single letter
		[[ ${selection} =~ $is_letter ]] || { clear_menu 8; continue; }

		case "${option}" in
			d)
				read -p "${T}Enter new description: ${W}" description
				clear_menu 12
				;;
			e)
				line_count=0
				while :; do
					read -p "${T}Enter new edition number: ${W}" edition

					# Error check: input is a number >= zero
					if [[ ${edition} =~ $is_num ]]; then
						clear_menu $(( 1 + $line_count ))
						break
					fi
					echo "${R} Invalid input."
					(( line_count+=2 ))	
				done
				clear_menu 12
				;;
			u)
				read -p "${T}Enter new CID: ${W}" cid_value
				clear_menu 12
				;;
			j)
				read -p "${T}Enter new hidden CID: ${W}" hidden_cid_value
				clear_menu 12
				;;
			g)
				#Build our args string for metadata_engine and run it (uses tokenizr.cfg defaults if no user input used)
				clear_menu 11
				echo "${G}[+] ${W}Applying metadata changes..."
				bin/metadata_engine -d "${description}" -e ${edition} -u ${cid_value} -j ${hidden_cid_value} >&/dev/null
				sleep 2
				clear_menu 2
				return
				;;
			b)
				clear_menu 11
				break
				;;
			?)
				clear_menu 11
				continue
				;;
		esac
	done
}

quick_package() {
	echo "${G}[+] ${W}- Making tar.gz file."
	sleep 2
	clear_menu 2
	util/make_distro
}

get_latest() {
	line_count=0
	upgrade='n'
	while :; do

		# Check for upgrade, then run get_latest which uses git to download latest tokenizr and transfers art assets to new tokenizr
		read -p "${T}Upgrade to the latest version of tokenizr? (y/N): ${W}" upgrade
		
		[ ${upgrade} = 'n' ] || [ ${upgrade} = 'N' ] && { clear_menu $(( 2 + $line_count )); echo "${R}[-] ${W}- Upgrade aborted. Returning to main menu."; sleep 2; clear_menu 3; return; }
		[ ${upgrade} = 'y' ] || [ ${upgrade} = 'Y' ] && { clear_menu $(( 2 + $line_count )); echo "${G}[+] ${W}- Using git to obtain latest version of tokenizr"; sleep 2; clear_menu 3; util/get_latest >&/dev/null; cd ../tokenizr; return; }

		echo "${upgrade}: option invalid."
		(( line_count+=2 ))
		upgrade='n'
	done
}

main