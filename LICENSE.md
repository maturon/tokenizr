Distributed under the GPLv3 License

Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.

For more information:
https://choosealicense.com/licenses/gpl-3.0/
